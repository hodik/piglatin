import 'mocha';
import {expect} from 'chai';
import {translateWordToPiglatin, translateTextToPiglatin} from '../src';

describe('English-to-Piglatin translator', () => {
    it('should transform words that start with a consonant so their first letter moved to the end of the word and the letters “ay” added to the end', () => {
        expect(translateWordToPiglatin('Hello')).to.be.equal('Ellohay');
        expect(translateWordToPiglatin('hello')).to.be.equal('ellohay');
    });

    it('should transform words that start with a vowel so the letters “way” added to the end', () => {
        expect(translateWordToPiglatin('apple')).to.be.equal('appleway');
        expect(translateWordToPiglatin('Apple')).to.be.equal('Appleway');
    });

    it('should not transform words that end in “way”', async() => {
        expect(translateWordToPiglatin('stairway')).to.be.equal('stairway');
    });

    it('should preserve punctuation within a word', () => {
        expect(translateWordToPiglatin('can’t')).to.be.equal('antca’y');
    });

    it('should transform text so hyphens are treated as two words', () => {
        expect(translateTextToPiglatin('this-thing')).to.be.equal('histay-hingtay');
    });

    it('shoud preserve capitalization within a word', () => {
        expect(translateWordToPiglatin('Beach')).to.be.equal('Eachbay');
        expect(translateWordToPiglatin('McCloud')).to.be.equal('CcLoudmay');
    });

    it('should preserve punctuation', () => {
        expect(translateTextToPiglatin('end.')).to.be.equal('endway.');
    });

    [
        ['I am from ...', 'Iway amway romfay ...'],
        ['Yes, a little', 'Esyay, away ittlelay'],
        ['Don’t tell them', 'Ontda’y elltay hemtay'],
        ['How do you say ... in Pig Latin?', 'Owhay oday ouyay aysay ... inway Igpay Atinlay?'],
    ].forEach((pair) => {
        it(`should translate '${pair[0]}' into '${pair[1]}`, () => {
            expect(translateTextToPiglatin(pair[0])).to.be.equal(pair[1]);
        });
    });
});