const SEPARATORS = `.,?!"':;- \t`;
const CONSONANTS = 'bcdfghjklmnpqrstvxzwy';
const VOWELS = 'aeiou';
const ALPHABET = (CONSONANTS + VOWELS) + (CONSONANTS + VOWELS).toUpperCase();
const CONSONANT_CASE_REGEXP = new RegExp(`^([${CONSONANTS}])(.*)$`, 'i')
const VOWEL_CASE_REGEXP = new RegExp(`^[${VOWELS}]`, 'i');
const NON_ALPHABET_CHARS_REGEXP = new RegExp(`[^${ALPHABET}]`, 'g');

/*
 * Return a list of indices of letters that are capital
 */
function getCapitalLetterPositions(word: string): number[] {
    const positions = [];

    for(let position = 0; position < word.length; position++) {
        const char = word[position];
        if(char === char.toUpperCase() && ALPHABET.includes(char)) {
            positions.push(position);
        }
    }

    return positions;
}

/*
 * Capitalizes letters of `word` at given `positions`.
 */
function capitalizeLetters(word: string, positions: number[]): string {
    return word
        .split('')
        .map((character, index) => {
            if(positions.includes(index)) {
                return character.toUpperCase();
            }

            return character;
        })
        .join('');
}

interface CharLocation {
    char: string;
    position: number;
}


function getNonLetterCharLocation(word: string): CharLocation {
    const locations: CharLocation[] = [];

    for(let index = 0; index < word.length; index++) {
        if(!ALPHABET.includes(word[index])) {
            locations.push({
                char: word[index],
                position: index
            })
        }
    }

    if(locations.length > 1) {
        throw new Error(`'${word}' contains more then 1 non-letter character`);
    }

    return locations[0];
}

function putCharFromTheEnd(word: string, location: CharLocation): string {
    return word.slice(0, word.length - location.position) + location.char + word.slice(word.length - location.position);
}

/*
 * Expects a single word with no punctuations
 */
export function translateWordToPiglatin(word: string): string {
    if(!word) {
        return word;
    }

    const normalizedWord = word.replace(NON_ALPHABET_CHARS_REGEXP, '');
    let translatedWord = '';

    // '-way' case
    if (normalizedWord.endsWith('way')) {
        translatedWord = normalizedWord;
    }
    // Word start wit a vowel
    else if(word.match(VOWEL_CASE_REGEXP) !== null) {
        translatedWord = `${normalizedWord}way`;
    }
    // Word starts with a consonant
    else {
        const consonantMatches = normalizedWord.match(CONSONANT_CASE_REGEXP);
        if(normalizedWord !== null) {
            translatedWord = `${consonantMatches[2]}${consonantMatches[1]}ay`;
        }
    }

    // Preserve capitalization
    translatedWord = translatedWord.toLowerCase();
    translatedWord = capitalizeLetters(translatedWord, getCapitalLetterPositions(word));

    // Preserve punctuation
    const location = getNonLetterCharLocation(word);
    if(location) {
        const locationFromThenEnd = {
            ...location,
            position: word.length - 1 - location.position
        };

        translatedWord = putCharFromTheEnd(translatedWord, locationFromThenEnd);
    }

    return translatedWord;
}

function splitIntoTokens(text: string): string[] {
    if(text.length === 0) {
        return [];
    }

    const tokens = [];
    let currentToken = '';
    let index = 0;
    while(index < text.length) {
        const char = text[index];
        if(SEPARATORS.includes(char)) {
            tokens.push(currentToken);
            tokens.push(char);
            currentToken = '';
        } else {
            currentToken += char;
        }

        index++;
    }

    tokens.push(currentToken);
    return tokens;
}

/*
 * Takes a text, splits into tokens and translates them one-by-one.
 */
export function translateTextToPiglatin(text: string): string {
    return splitIntoTokens(text).map((token) => {
        return !SEPARATORS.includes(token) ? translateWordToPiglatin(token) : token;
    }).join('');
}